#!/bin/bash
pandoc  --toc \
	--toc-depth=2 \
	--highlight-style=tango \
	--reference-doc=style/numbered-sections.docx \
	--lua-filter=filter/pagebreak.lua \
	-s  src/content.md \
	-o out/content.docx

