# Pandoc MD to DOCX

Demo repository to generate docx from md file, with proper formatting for a report.

The final report is [here][report_docx].

Blog post is [here][blog_post].

## Directory structure



- [src](src) - documents
- [filter](filter) - pandoc filters
- [out](out)- output directory
- [style](style) - reference-doc



[report_docx]: out/report.docx
[blog_post]: https://warenix.gitlab.io/blog/post/2020-09-05-convert-md-to-docx-using-pandoc/