# Convert markdown to docx using pandoc



Using Word is hard! Especially when you need to deal with text formatting, numbering and spacing.

Heard of pandoc for some time but I haven't tried it until today. At first it works amazingly well to generate a docx file from a md file.

```sh
pandoc -t a.md -o a.docx
```

Having the a.docx isn't sufficient to send it out, I still need to add a cover page, table of contents, headers, footers and at least change the font type and color.



At the end I come up with this workflow

1. Prepare cover page in `cover.docx`
2. Write content in `content.md`
3. Use pandoc to convert `content.md ` to `content.docx`
4. Manually add back content in `cover.docx` to the first page of `content.docx` and save as `report.docx`



## Cover Page

I attempt to add a cover page before table of contents but it actually doesn't work, the generated table of content always comes at the first page. At the end I come up a manual solution:

1. prepare the cover page in `cover.docx`, edit it with Word
2. copy and paste content of `cover.docx` to the generated `content.docx` 

## Table of Content

Table of content is generated automatically by pandoc with two arguments

- `--toc`: generate table of contents (but shown at the first page...)
- `--toc-depth`: controls how deep is the table of contents

## Headers, footers and text styles

For outputting `.docx` the only way to control styles is use the `--reference-doc`.



As I like each section to be numbered so I use the [numbered-sections.docx][https://github.com/krissen/pandoc-extra/blob/master/templates/numbered-sections.docx] from [pandoc wiki][pandoc_wiki] as the base file and modify it.

- header: added the report title
- footer: added page number
- modified table header style to not inherit from `H1` but `Body text`
- modified `H1` to add a page break before starting



## Build doc

This command will generate `content.docx`

```sh
#!/bin/bash
pandoc  --toc \
        --toc-depth=2 \
        --highlight-style=tango \
        --reference-doc=style/numbered-sections.docx \
        --lua-filter=filter/pagebreak.lua \
        -s  src/content.md \
        -o out/content.docx
```

Note:

1. [pagebreak.lua][pagebreak.lua]: newpage in markdown


[pandoc_wiki]: https://github.com/jgm/pandoc/wiki/User-contributed-templates

[pagebreak.lua]: https://github.com/pandoc/lua-filters/tree/master/pagebreak

